import { TestBed, inject } from '@angular/core/testing';

import { User.Login.ServiceService } from './user.login.service.service';

describe('User.Login.ServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [User.Login.ServiceService]
    });
  });

  it('should be created', inject([User.Login.ServiceService], (service: User.Login.ServiceService) => {
    expect(service).toBeTruthy();
  }));
});

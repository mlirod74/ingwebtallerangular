import { Component, OnInit } from '@angular/core';
import { Tarea, EstadoTarea } from './tarea';
import { UserLogin } from './user.login';
import { TareaService } from './tarea.service';
import { UserLoginService } from './user.login.service';
import { catchError } from 'rxjs/operators';
import * as L from 'leaflet';
import { isNull } from 'util';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {
  title = 'Todo Listo!';
  estadoTareas = EstadoTarea;
  tareaSeleccionada: Tarea; // Referencia a la tarea del listado para promover su estado.
  tareaParaModificar: Tarea; // Referencia a la tarea del listado para modificar por formulario.
  mostrarMapa: Array<Tarea>;
  tareas: Array<Tarea>; // Listado de tareas para rederizar.
  newTarea: Tarea; // Información con la nueva tarea que sera almacenada en el back-end.
  userLogin: UserLogin; // Objeto para mantener la información del login del ususario.
  errorToShow: string; // Para asignar errores que seran desplegados en la página.
  options;
  markers: L.Layer[] = [];
  markers2: L.Layer[] = [];

  constructor(private tareaService: TareaService, private userLoginService: UserLoginService) {
    this.tareas = [];
    this.newTarea = new Tarea(null, null, null);
    this.errorToShow = '';
    const uljson = localStorage.getItem('userLogin');
    if (uljson) {
        this.userLogin = JSON.parse(uljson);
    } else {
        this.userLogin = new UserLogin();
    }
  }

  /**
   * Función que se ejecuta cada vez que se despliega la página html asociada a este componente.
   *
   */
  ngOnInit() {
        if ( this.userLogin.isLogged ) {
            this.reloadTareas();
        }
        this.deseleccionarTareas();
  }

  /**
   * Función que recupera el listado de tareas desde el back-end para renderizarlo en la página html.
   *
   */
  private reloadTareas() {
            this.options = {
              layers: [
                L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
              ],
              zoom: 15,
              center: L.latLng(-33.0454915, -71.6124715),
            };
            this.tareaService.getTareas(this.userLogin.token)
            .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
            .subscribe((ts: Array<Tarea>) => { this.errorToShow = ''; this.tareas = ts; });
            $('html, body').animate({scrollTop:0}, 'slow');
  }


  /**
   * Función que referencia una tarea seleccionada, desde la página html, para poder promover su estado.
   *
   */

  mapClick(evt) {
    this.addMarker(evt['latlng']);
  }


  addMarker(latlng) {
    this.newTarea.latitud = latlng['lat'];
    this.newTarea.longitud = latlng['lng'];
    let newMarker = L.marker([latlng['lat'],  latlng['lng']], {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl:   'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
     })
    });

    while ( this.markers.length > 0 ) {
      this.markers.pop();
    }
    this.markers.push(newMarker);
  }

  addMarker2(t: Tarea) {
    let newMarker = L.marker([ Number(t.latitud),  Number(t.longitud)], {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 41 ],
        iconUrl:   'assets/marker-icon.png',
        shadowUrl: 'assets/marker-shadow.png'
      })
     });
     let ini = t.fecha_inicio;
     let fin = t.fecha_termino;
     if ( isNull(t.fecha_inicio )) {
      let ini = '--';
     }
     if ( isNull(t.fecha_termino )) {
       let fin = '--';
     }
     newMarker.bindPopup('Descripcion: ' + t.descripcion + '<br/>' + ' Estado :' + t.nombre_estado
                         + '<br/>' + ' Fecha inicio :' + ini + '<br/>' + ' Fecha Fin :' + fin);
     this.markers2.push(newMarker);
   }

  /**
   * Función que referencia una tarea seleccionada, desde la página html, para poder promover su estado.
   *
   */
  mostrarMapaGeo() {
    this.tareaParaModificar = null;
    this.tareaSeleccionada = null;
    for (let i of this.tareas) {
      if ( i.latitud ) {
        // console.log(i);
        this.addMarker2(i);
     }
    }
    this.options = {
      layers: [
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
      ],
      zoom: 15,
      center: L.latLng(-33.0454915, -71.6124715),
    };
    this.mostrarMapa = this.tareas;
  }

   onMapReady(map: L.Map) {
    setTimeout(() => {
      map.invalidateSize();
    }, 0);
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
   }

  /**
   * Función que referencia una tarea seleccionada, desde la página html, para poder promover su estado.
   *
   */
  seleccionarTarea(t: Tarea) {
    this.tareaParaModificar = null;
    this.mostrarMapa = null;
    this.tareaSeleccionada = t;
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
  }

  /**
   * Función que referencia una tarea seleccionada, desde la página html, para poder modificar sus atributos.
   *
   */
  seleccionarTareaModificar(t: Tarea) {
    this.tareaSeleccionada = null;
    this.tareaParaModificar = t;
    this.mostrarMapa = null;
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
   }

  /**
   * Función que deseleccionada la tarea marcada en la página html y remueve su instancia.
   *
   */
  deseleccionarTareas() {
      this.tareaSeleccionada = null;
      this.tareaParaModificar = null;
  }

  /**
   * Función para crear una nueva tarea, almacenarla en el back-end, y renderizar en la página html.
   *
   */
  crearTarea(userLogin: UserLogin) {
    console.log(this.userLogin.token);
    this.newTarea.id = Math.max.apply(null, this.tareas.map(x => x.id)) + 1;
    this.newTarea.estado = EstadoTarea.Creada;
    this.newTarea.nombre_estado = this.estado2str(EstadoTarea.Creada);
    this.newTarea.username = userLogin.username;
    this.tareaService.crearTarea(this.newTarea, userLogin.token)
                     .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
                     .subscribe((t: Tarea) => { this.errorToShow = ''; this.newTarea = new Tarea(null, '', '');
                                                this.tareas.push(t); });
  }

  /**
   * Función para promover el estado de una tarea, almacenar el cambio en el back-end, y actualizarlo en la página html.
   *
   */
  siguienteEstadoTarea(tarea: Tarea) {
      tarea.nombre_estado = this.estado2str(tarea.estado);
      this.tareaService.actualizarTarea(tarea, this.userLogin.token)
                        .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
                        .subscribe((t: Tarea) => { this.errorToShow = ''; tarea = t; });
  }

  /**
   * Función para autenticar un usuario basada en username y password, y generar un token de sesión.
   *
   */
  iniciarSesion(userLogin: UserLogin) {
    this.userLoginService.login(userLogin)
                        .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
                        .subscribe(resp => { this.errorToShow = ''; userLogin.password = ''; userLogin.token = resp['key'];
                            userLogin.isLogged = true; localStorage.setItem('userLogin', JSON.stringify(userLogin) );
                            this.reloadTareas();
                        });
  }

  /**
   * Función para borrar el token de sesión en el back-end y front-end.
   *
   */
  cerrarSesion(userLogin: UserLogin) {
    this.userLoginService.logout(userLogin)
                    .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
                    .subscribe(resp => { this.errorToShow = ''; userLogin.username = ''; userLogin.token = '';
                                userLogin.isLogged = false; localStorage.removeItem('userLogin'); });
  }




  estado2str(e: EstadoTarea) {
    switch (e) {
      case EstadoTarea.Creada:    return 'Creada';
      case EstadoTarea.EnProceso: return 'En Proceso';
      case EstadoTarea.Terminada: return 'Terminada';
    }
  }

}

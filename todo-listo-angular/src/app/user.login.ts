export class UserLogin {
    username: string; // Nombre de login del usuario.
    password: string; // Contraseña del usuario.
    token: string; // Token de la sesión de usuario.
    isLogged: boolean; // Si el usuario esta con un token de sesión valido.

    constructor() {
        this.isLogged = false;
    }
}

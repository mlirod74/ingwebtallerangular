import { Injectable } from '@angular/core';
import { UserLogin } from './user.login';
import { Observable, } from 'rxjs';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import {environment} from '../environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserLoginService {
    private headers; // Para agregar las cabeceras HTTP Content-Type y Authorization.

  constructor(private http: HttpClient) {
      this.headers = new HttpHeaders({'Content-Type':  'application/json'});
  }

  /**
   * Función para autenticar un usuario basada en username y password, y generar un token de sesión
   * en el back-end.
   *
   */
    login(userLogin: UserLogin): Observable<any> {
        return this.http.post(environment.URL_LOGIN, userLogin)
                .pipe(catchError(err => { console.log(`ERROR login: ${err.message}`); throw err; }));
    }

  /**
   * Función para borrar el token de sesión en el back-end.
   *
   */
    logout(userLogin: UserLogin): Observable<any> {
        this.headers = this.headers.set('Authorization', `Token ${userLogin.token}`);
        return this.http.post(environment.URL_LOGOUT, userLogin, { headers: this.headers })
                .pipe(catchError(err => { console.log(`ERROR logout: ${err.message}`); throw err; }));
    }

}

import { Input, Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Tarea } from '../tarea';
import { UserLogin } from '../user.login';
import { TareaService } from '../tarea.service';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-modificar-tarea',
  templateUrl: './modificar-tarea.component.html',
  styleUrls: ['./modificar-tarea.component.css']
})
export class ModificarTareaComponent implements OnInit {
    @Input() tarea: Tarea; // La tarea referenciada desde la página html.
    @Input() userLogin: UserLogin; // Objeto para mantener la información del login del ususario.
    errorToShow: string; // Para asignar errores que seran desplegados en la página.

  constructor(private tareaService: TareaService) { }

  ngOnInit() {
  }

  /**
   * Función para modificar los atributos de una tarea, almacenar estos cambios en el back-end,
   * y actualizarlos en la página html.
   *
   */
  modificarTarea() {
      if ( this.tarea.fecha_inicio != null ) { this.tarea.fecha_inicio =  new Date(this.tarea.fecha_inicio).toISOString(); }
      if ( this.tarea.fecha_termino != null ) { this.tarea.fecha_termino = new Date(this.tarea.fecha_termino).toISOString(); }
      if ( this.tarea.fecha_inicio >  this.tarea.fecha_termino) {
            this.errorToShow = 'Error: Fecha de inicio de tarea mayor o igual a fecha de término de tarea';
            return;
      }
      this.tareaService.actualizarTarea(this.tarea, this.userLogin.token)
                       .pipe(catchError(err => { this.errorToShow = err.message; return err; }))
                       .subscribe((t: Tarea) => { this.errorToShow = ''; this.tarea = t; });
  }
}

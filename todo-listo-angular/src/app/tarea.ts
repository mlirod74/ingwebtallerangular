export enum EstadoTarea {
    Creada
    , EnProceso
    , Terminada
}

export function estado2str(e: EstadoTarea): string {
    switch (e) {
      case EstadoTarea.Creada: return 'Creada';
      case EstadoTarea.EnProceso: return 'En Proceso';
      case EstadoTarea.Terminada: return 'Terminada';
    }
  }

export class Tarea {
    id: number; // ID de la tarea
    titulo: string; // Título (nombre) de la tarea
    descripcion; // Descripción de la tarea.
    estado: number; // ID de estado de la tarea
    nombre_estado: string; // Nombre del estado de la tarea.
    fecha_inicio: string; // Fecha de inicio de la tarea.
    fecha_termino: string; // Fecha de término de la tarea.
    username: string; // Nombre de login del usuario asociado a la tarea.
    latitud: string; // Nombre de login del usuario asociado a la tarea.
    longitud: string; // Nombre de login del usuario asociado a la tarea.


    constructor(id, titulo, descripcion, estado = EstadoTarea.Creada) {
        this.id = id;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    toString() {
        return `Tarea #${this.id}: ${this.titulo}`;
    }
}

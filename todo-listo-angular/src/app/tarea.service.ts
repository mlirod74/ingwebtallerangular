import { Injectable } from '@angular/core';
import { Tarea } from './tarea';
import { Observable, } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import {environment} from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class TareaService {
    private headers; // Para agregar las cabeceras HTTP Content-Type y Authorization.

    constructor(private http: HttpClient) {
        this.headers = new HttpHeaders({'Content-Type':  'application/json'});
    }

  /**
   * Función para recuperar el listado de tareas, asociadas a un usuario, desde el back-end.
   *
   */
    getTareas(token: string): Observable<any> {
        this.headers = this.headers.set('Authorization', `Token ${token}`);
        return this.http.get(environment.URL_TAREAS, { headers: this.headers })
                .pipe(catchError(err => { console.log(`ERROR getTareas: ${err.message}`); throw err; }));
    }

  /**
   * Función para crear una nueva tarea, asociada a un usuario, en el back-end.
   *
   */
    crearTarea(t: Tarea, token: string): Observable<any> {
        this.headers = this.headers.set('Authorization', `Token ${token}`);
        return this.http.post(environment.URL_TAREAS, t, { headers: this.headers })
                .pipe(catchError(err => { console.log(`ERROR crearTarea: ${err.message}`); throw err; }));

    }

  /**
   * Función para actualizar la información de una tarea en el back-end.
   *
   */
    actualizarTarea(t: Tarea, token: string): Observable<any> {
        this.headers = this.headers.set('Authorization', `Token ${token}`);
        return this.http.put(environment.URL_TAREAS + t.id + '/', t, { headers: this.headers })
                .pipe(catchError(err => { console.log(`ERROR actualizarTarea: ${err.message}`); throw err; }));
    }
}














"""todolisto_backend URL Configuration
"""
from django.contrib import admin
from django.urls import path, include
from backend.models import Tarea, Estado
from rest_framework import routers, serializers, viewsets
from rest_framework import permissions, authentication

class EstadoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Estado
        fields = ('id', 'nombre')

class EstadoViewSet(viewsets.ModelViewSet):
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer

class TareaSerializer(serializers.HyperlinkedModelSerializer):
    nombre_estado = serializers.ReadOnlyField()
    estado = serializers.PrimaryKeyRelatedField(queryset=Estado.objects.all(), required=True)
                    
    class Meta:
        model = Tarea
        fields = ('id', 'titulo', 'descripcion', 'fecha_inicio', 'fecha_termino', 'estado', 'nombre_estado', 'username', 'latitud', 'longitud')        

class TareaViewSet(viewsets.ModelViewSet):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TareaSerializer
    
    def get_queryset(self): # Retorna un queryset de las tareas de un usuario especifico. 
       return Tarea.objects.filter(username = self.request.user.username)        

router = routers.DefaultRouter()
router.register(r'tareas', TareaViewSet, 'Tarea')
router.register(r'estados', EstadoViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),    
]
